let id = "";

window.onload = function () {
    const urlParams = new URLSearchParams(location.search);
    const deleteCourse = document.getElementById("deleteCourse");

    if (urlParams.has("courseid") === true) {
        id = urlParams.get("courseid");
        loadDeleteDetails(id);       
    }

    deleteCourse.onclick = deleteCourseBtnClicked;
};

function loadDeleteDetails(id){
    document.getElementById("courseInfo").innerHTML = "Course ID: " + id;
}

function deleteCourseBtnClicked(event) {
    let deleteCourseURL =
    `http://localhost:8081/api/courses/`;

    fetch(deleteCourseURL + id, {method: 'DELETE'})
        .then(response => {
            if (response.status >= 200 && response.status < 300) {
                let confirmationMessage =
                document.getElementById("deleteConfirmationMessage");
            confirmationMessage.innerHTML = "Course ID " + id + " deleted";
            }else{
                console.log("Error returned with status code: " + response.status);
                let confirmationMessage =
                document.getElementById("deleteConfirmationMessage");
            confirmationMessage.innerHTML = "Error returned with status code: " + response.status;
            }
            return response.json();
        })
        .then(data => {
            // and display returned data in a div
            //event.preventDefault();
            console.log(data);
            location.assign('/index.html');
            let confirmationMessage =
            document.getElementById("deleteConfirmationMessage");
        confirmationMessage.innerHTML = "Course ID " + id + " deleted";
        return false;
        })
        .catch(err => {
        // If the DELETE returns an error, display a message
        console.log(err);
        let confirmationMessage =
        document.getElementById("deleteConfirmationMessage");
        confirmationMessage.innerHTML = "Unexpected error: " + err;
        location.assign('/index.html');
    });

    return false;
}

// function btnGetClicked() {
//     // fetch student to decide if it should
//     // be deleted
//     id = document.getElementById("stuIdField").value;
//     fetch("http://www.some-url.com/api/student/" + id)
//         .then(response => response.json())
//         .then(data => {
//             // and display returned data in a div
//             let stuDetails = "ID: " + data.id +
//                 " Name: " + data.name +
//                 " Classification: " + data.classification;
//             let studentDetailsDiv =
//                 document.getElementById(studentDetailsDiv);
//             studentDetailsDiv.innerHTML = stuDetails;
//         });
// }

// function btnDeleteClicked() {
//     // send DELETE request w/ id as part of URL
//     fetch("http://www.some-url.com/api/students/" + id, 
// {
//             method: "DELETE"
//         })
//         .then(response => response.json())
//         .then(json => {
//             // If the DELETE is successful, display a message
            // let confirmationMessage =
            //     document.getElementById(confirmationMessage);
            // confirmationMessage.innerHTML = "Student deleted";
//         });
//     .catch(err => {
//         // If the DELETE returns an error, display a message
        // let confirmationMessage =
        //     5 - 14
        // document.getElementById(confirmationMessage);
        // confirmationMessage.innerHTML = "Unexpected error.";
//     });
// }