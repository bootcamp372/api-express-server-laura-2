window.onload = function () {
    const urlParams = new URLSearchParams(location.search);

    let id = -1;
    if (urlParams.has("courseid") === true) {
        id = urlParams.get("courseid");
        // Now that you know the course id, make an
        // AJAX call to get that one course
        // and in the callback, display it.
        loadCourseDetails(id);
    }else{
        loadCoursesTable();
    }
};

function loadCoursesTable() {
    let allCoursesURL =
        `http://localhost:8081/api/courses`;
    const tbody = document.getElementById("coursesTblBody");
    clearValues(tbody);
    fetch(allCoursesURL)
        .then(response => response.json())
        .then(jsonData => {
            for (let i = 0; i < jsonData.length; i++) {
                detailsUrl = `http://localhost:5500/details.html?courseid=${jsonData[i].id}`;
                displayCourses(jsonData[i], detailsUrl);
            }
        })
        .catch(err => console.error(err));
}

function loadCourseDetails(id) {
    let courseURL =
        `http://localhost:8081/api/courses/` + id;
    const tbody = document.getElementById("courseDetailTblBody");
    clearValues(tbody);
    fetch(courseURL)
        .then(response => response.json())
        .then(jsonData => {
                displayCourseDetail(jsonData);
        })
        .catch(err => console.error(err));
}

function clearValues(tbody) {
    if (tbody){
        tbody.innerHTML = "";
    }   
}

function displayCourses(course, detailsUrl) {
    const coursesTblBody = document.getElementById("coursesTblBody");
    if (coursesTblBody) {
        let row = coursesTblBody.insertRow(-1);
        let cell1 = row.insertCell(0);
        let cell2 = row.insertCell(1);
        let cell3 = row.insertCell(2);

        cell1.innerHTML = course.dept;
        cell2.innerHTML = "<a href=" + detailsUrl + ">" + course.courseName + "</a>";
        cell3.innerHTML = course.courseNum;

        let deleteLink =  document.createElement('a'); 
        deleteUrl = `http://localhost:5500/confirm-delete.html?courseid=${course.id}`;
        deleteLink.href = deleteUrl;
        //deleteLink.target = "_blank";
        deleteLink.innerHTML = "Delete " + course.courseName;

        row.appendChild(deleteLink);

    }

}

function displayCourseDetail(course) {
    const courseDetailTblBody = document.getElementById("courseDetailTblBody");
    const courseHeading = document.getElementById("courseHeading");
    if (courseHeading) { 
        courseHeading.innerHTML = "Course Detail - " + course.courseName;
    }
    

    if (courseDetailTblBody) {
        let row = courseDetailTblBody.insertRow(-1);
        let cell1 = row.insertCell(0);
        let cell2 = row.insertCell(1);
        let cell3 = row.insertCell(2);

        cell1.innerHTML = course.instructor;
        cell2.innerHTML = course.startDate;
        cell3.innerHTML = course.numDays;

    }
}